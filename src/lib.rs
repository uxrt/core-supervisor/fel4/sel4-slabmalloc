//! Copyright (c) 2019-2020 Andrew Warkentin
//! Copyright (c) 2017 Ryan Breen
//! Copyright (c) 2015-2019 Gerd Zellweger
//!
//! Licensed under the MIT license <LICENSE-MIT or 
//! http://opensource.org/licenses/MIT>.
//! All files in the project carrying such notice may not be copied, modified, 
//! or distributed except according to those terms.

//! This is a ported version of https://github.com/gz/rust-slabmalloc
//! to integrate with the memory infrastructure in UX/RT.
//!
//! A slab allocator implementation for small objects
//! (< architecture page size).
//!
//! The organization is as follows (top-down):
//!
//!  * A `ZoneAllocator` manages many `SlabAllocator` and can
//!    satisfy requests for different allocation sizes.
//!  * A `SlabAllocator` allocates objects of exactly one size.
//!    It holds its data in a SlabList.
//!  * A `SlabPage` contains allocated objects and associated meta-data.
//!  * A `SlabPageProvider` is provided by the client and used by the
//!    SlabAllocator to allocate SlabPages.
//!


//TODO: integrate the object slab allocator at https://crates.io/crates/slab-alloc (source at https://github.com/ezrosent/allocators-rs/tree/master/slab-alloc)  on top of the slabmalloc heap allocator; it will need some porting - see https://github.com/ezrosent/allocators-rs/issues/2 (it is likely that only a source of timestamps will need to be provided; the custom backing allocator support mentioned there probably doesn't need to be implemented since Vec and Box are working); eventually, the two slab allocators should be merged (although allocating unstructured blocks will probably have to always be supported for vectors and boxes)

#![no_std]
#![feature(allocator_api)]
extern crate wee_alloc;
extern crate sel4_alloc;

#[macro_use]
extern crate sel4;

#[macro_use]
extern crate alloc;

#[macro_use]
extern crate sel4_sys;

mod slab_alloc;

use alloc::boxed::Box;
use core::alloc::{Alloc, AllocErr, GlobalAlloc, Layout};
use core::ptr::NonNull;
use sel4_alloc::bootstrap::BootstrapAllocatorBundle;

use slab_alloc::ZoneAllocator;

static mut KOBJ_ALLOC: Option<BootstrapAllocatorBundle> = None;
static mut SLAB_ALLOC: Option<ZoneAllocator> = None;

//TODO: reduce the size of the bootstrap heap once the slab allocator is working
static mut WEE_ALLOC: wee_alloc::WeeAlloc = wee_alloc::WeeAlloc::INIT;
static mut BOOT_ALLOC_FREE: bool = true;

static mut EXEC_START: usize = 0;
static mut EXEC_END: usize = core::usize::MAX;

pub trait AllocatorMutex {
	fn lock(&self);
	fn unlock(&self);
}

struct DummyAllocMutex {
}

impl AllocatorMutex for DummyAllocMutex {
	fn lock(&self) {
	}
	fn unlock(&self) {
	}
}
	
static mut ALLOC_MUTEX: Option<Box<AllocatorMutex>> = None;

pub unsafe fn init(alloc: BootstrapAllocatorBundle) {
	KOBJ_ALLOC = Some(alloc);
	SLAB_ALLOC = Some(ZoneAllocator::new(KOBJ_ALLOC.as_mut().unwrap()));
}

pub unsafe fn init_mutex(mutex: Box<dyn AllocatorMutex + 'static>)
{
	ALLOC_MUTEX = Some(mutex);
}

unsafe fn get_mutex() -> &'static mut Box<dyn AllocatorMutex + 'static>
{
	if ALLOC_MUTEX.is_none() {
		ALLOC_MUTEX = Some(Box::new(DummyAllocMutex{}));
	}
	ALLOC_MUTEX.as_mut().unwrap()
}

pub struct Allocator;

unsafe impl<'a> Alloc for &'a Allocator {
	unsafe fn alloc(&mut self, layout: Layout) -> Result<NonNull<u8>, AllocErr> {
		if SLAB_ALLOC.is_none() {
			bootstrap_allocate(layout)
		} else {
			slab_allocate(layout)
		}
	}

	unsafe fn dealloc(&mut self, ptr: NonNull<u8>, layout: Layout) {
		//TODO: finish this
	}
	unsafe fn realloc(
			&mut self,
			_ptr: NonNull<u8>,
			_old_layout: Layout,
			_new_size: usize,
		) -> Result<NonNull<u8>, AllocErr> {
		//TODO: finish this
		Err(AllocErr)
	}

}

unsafe impl GlobalAlloc for Allocator {
	unsafe fn alloc(&self, layout: Layout) -> *mut u8 {
		let mutex = get_mutex();
		if BOOT_ALLOC_FREE {
			match bootstrap_allocate(layout){
				Ok(ret) => {
					return ret.as_ptr()
				},
				Err(_) => {
					if SLAB_ALLOC.is_none(){
						panic!("bootstrap heap exhausted without main heap allocator initialized");
					}
					//quit allocating from the bootstrap
					//heap until something from it is freed
					mutex.lock();
					BOOT_ALLOC_FREE = false;
					mutex.unlock();
				},
			}
		}
		mutex.lock();
		let ret = slab_allocate(layout).unwrap().as_ptr();
		mutex.unlock();
		return ret;
	}

	unsafe fn dealloc(&self, ptr: *mut u8, layout: Layout) {
		let mutex = get_mutex();
		if is_scratch(ptr) {
			//try to allocate from the bootstrap heap on the next
			//allocation, since it will have free space after this
			mutex.lock();
			BOOT_ALLOC_FREE = true;
			mutex.unlock();
			bootstrap_deallocate(ptr, layout);
		} else {
			mutex.lock();
			slab_deallocate(ptr, layout);
			mutex.unlock();
		}
	}
}

unsafe fn bootstrap_allocate(layout: Layout) -> Result<NonNull<u8>, AllocErr> {
	let ret = WEE_ALLOC.alloc(layout);
	if ret.is_null(){
		Err(AllocErr)
	}else{
		Ok(NonNull::new(ret).unwrap())
	}

}

unsafe fn bootstrap_deallocate(ptr: *mut u8, layout: Layout) {
	WEE_ALLOC.dealloc(ptr, layout)
}

unsafe fn slab_allocate(layout: Layout) -> Result<NonNull<u8>, AllocErr> {
	let ret = SLAB_ALLOC.as_mut().expect("SLAB_ALLOC unset").allocate(layout.size(), layout.align());
	if ret.is_none(){
		Err(AllocErr)
	}else{
		Ok(NonNull::new(ret.unwrap()).unwrap())
	}
}

unsafe fn slab_deallocate(ptr: *mut u8, layout: Layout) {
	SLAB_ALLOC.as_mut().expect("SLAB_ALLOC unset").deallocate(ptr, layout.size(), layout.align())
}


unsafe fn is_scratch(ptr: *const u8) -> bool {
	(ptr as usize) >= EXEC_START && (ptr as usize) < EXEC_END
}

pub unsafe fn set_exec_addrs(start: usize, end: usize)
{
	EXEC_START = start;
	EXEC_END = end;
}


/* vim: set softtabstop=8 tabstop=8 shiftwidth=8 noexpandtab: */
