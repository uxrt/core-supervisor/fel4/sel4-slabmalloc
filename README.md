# sel4-slabmalloc

This crate provides a heap allocator for feL4-based root servers, implemented on
top of sel4-alloc. It is able to provide bootstrap heap allocation before
sel4-alloc has been initialized by wrapping wee_alloc with the static array 
backend.

TODO: finish this
